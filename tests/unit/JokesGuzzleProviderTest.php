<?php

/* 
 * GuzzleAPIProviderTest
 */

namespace App\Tests\Util;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class JokesGuzzleProviderTest extends KernelTestCase
{

    private $service;

    public function setUp()
    {
        self::bootKernel();
        $this->service = self::$container->get('jokes.provider');
    }

    public function testGetCategories()
    {
        $this->assertNotNull($this->service->getCategories());
    }

    public function testGetRandomJoke()
    {
        $this->assertNotNull($this->service->getRandomJoke());
    }
}