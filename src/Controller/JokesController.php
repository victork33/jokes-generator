<?php namespace App\Controller;

use App\Form\IndexForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Contract\JokesInterface;
use App\Contract\ExportInterface;
use App\Service\JokeMailer;


class JokesController extends AbstractController
{
    /**
     * @param Request $request
     * @param JokesInterface $provider
     * @param ExportInterface $export
     * @param JokeMailer $mailer
     * @return mixed
     */
    public function index(
        Request $request,
        JokesInterface $provider,
        ExportInterface $export,
        JokeMailer $mailer
    ) {
        $categories = $provider->getCategories();
        if (!$categories) {
            return $this->render('error.html.twig', ['msg' => 'Categories List is Empty!']);
        }

        $options = ['data' => compact('categories')];
        $form = $this->createForm(IndexForm::class, null, $options);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $data = $form->getData();

                $joke = $provider->getRandomJoke($data['category']);
                if (!$joke) {
                    return $this->render('error.html.twig', ['msg' => 'Joke is not fetched!']);
                }
                $export->save($joke);
                $mailer->send($joke, $data['email']);

                return $this->redirectToRoute('submitted');
            } catch(\Exception $e) {
                echo $e->getMessage();
            }
        }

        return $this->render('index.html.twig', ['form' => $form->createView()]);
    }

}