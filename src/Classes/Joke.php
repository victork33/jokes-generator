<?php namespace App\Classes;

class Joke
{
    /** @var int  */
    private $id;

    /** @var string  */
    private $category;

    /** @var string  */
    private $text;

    /**
     * @param int $id
     * @param string $category
     * @param string $text
     */
    function __construct(int $id, string $category, string $text)
    {
        $this->id = $id;
        $this->category = $category;
        $this->text = $text;
    }

    /**
     * @return int
     */
    function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    function getCategory() {
        return $this->category;
    }

    /**
     * @return string
     */
    function getText() {
        return $this->text;
    }

}