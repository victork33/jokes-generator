<?php namespace App\Service;

use App\Classes\Joke;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class JokeMailer
{
    /** @var \Swift_Mailer  */
    private $mailer;

    /** @var EngineInterface  */
    private $templator;

    /**
     * @param \Swift_Mailer $mailer
     * @param EngineInterface $templator
     */
    function __construct(\Swift_Mailer $mailer, EngineInterface $templator)
    {
        $this->mailer = $mailer;
        $this->templator = $templator;
    }

    /**
     * @param Joke $joke
     * @param $email
     */
    public function send(Joke $joke, $email)
    {
        $message = (new \Swift_Message('Random Joke from ' . $joke->getCategory()))
            ->setTo($email)
            ->setBody(
                $this->templator->render(
                    'emails/joke.html.twig',
                    ['text' => $joke->getText(),]
                ),
                'text/html'
            );

        $this->mailer->send($message);
    }
}