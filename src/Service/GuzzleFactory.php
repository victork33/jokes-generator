<?php

namespace App\Service;

use GuzzleHttp\Client;

class GuzzleFactory
{
    public static function create()
    {
        return new Client();
    }
}