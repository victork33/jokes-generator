<?php namespace App\Service;

use App\Contract\JokesInterface;
use GuzzleHttp\Client;
use App\Classes\Joke;

class JokeGuzzleProvider implements JokesInterface
{
    const CATEGORIES_URL = '/categories';
    const JOKES_URL = '/jokes/random';

    /** @var Client  */
    private $client;

    /** @var string  */
    private $baseUrl;

    /**
     * @param Client $client
     * @param string $baseUrl
     */
    function __construct(Client $client, string $baseUrl)
    {
        $this->client = $client;
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $result = [];

        $url = $this->baseUrl . self::CATEGORIES_URL;

        $rq = $this->client->request('GET', $url);
        $content = json_decode($rq->getBody()->getContents());

        if ($content && property_exists($content, 'value')) {
            $result = $content->value;
        }

        return $result;
    }

    /**
     * @param string $category
     * @return Joke|null
     */
    public function getRandomJoke(string $category = ''): ?Joke
    {
        $result = null;

        $url = $this->baseUrl . self::JOKES_URL;
        if ($category) {
            $url = sprintf('%s/?limitTo=[%s]', $url, $category);
        }

        $rq = $this->client->request('GET', $url);
        $content = json_decode($rq->getBody()->getContents());

        if ($content &&
            property_exists($content, 'value') &&
            is_object($content->value) &&
            property_exists($content->value, 'joke') &&
            property_exists($content->value, 'id')
        ) {
            $result = new Joke($content->value->id, $category, $content->value->joke);
        }

        return $result;
    }
}