<?php 

namespace App\Service;

use App\Classes\Joke;
use App\Contract\ExportInterface;
use Symfony\Component\Filesystem\Filesystem;

class JokeExportToFile implements ExportInterface
{
    /** @var string  */
    private $storagePath;

    /**
     * @param string $storagePath
     */
    function __construct(string $storagePath)
    {
        $this->storagePath = $storagePath;
    }

    /**
     * @param Joke $joke
     */
    public function save(Joke $joke)
    {
        $pathParts = pathinfo($this->storagePath);
        $filesystem = new Filesystem();
        if (!$filesystem->exists($pathParts['dirname'])) {
            $filesystem->mkdir($pathParts['dirname'], 0700);
        }

        $content = $this->jokeToContent($joke);
        $filesystem->appendToFile($this->storagePath, $content);
    }

    /**
     * @param Joke $joke
     * @return string
     */
    private function jokeToContent(Joke $joke)
    {
        $content = 'Joke ' . $joke->getId() . PHP_EOL;
        if ($joke->getCategory()) {
            $content .= 'Category: ' . $joke->getCategory() . PHP_EOL;
        }
        $content .= $joke->getText() . PHP_EOL;
        $content .= '---' . PHP_EOL;

        return $content;
    }
}