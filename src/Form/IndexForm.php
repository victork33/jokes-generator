<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
 
class IndexForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options )
    {
        $categories = $this->categoriesToOptions($options['data']['categories']);

        $builder->add('category', ChoiceType::class, ['choices'  =>  $categories, 'label' => 'Category'])
            ->add('email', EmailType::class, ['required' => true, 'label' =>'E-mail']);
    }

    /**
     * @param $categories
     * @return array
     */
    private function categoriesToOptions($categories)
    {
        $res = [];
        foreach ($categories as $category) {
            $res[$category] = $category;
        }

        return $res;
    }

}