<?php

namespace App\Contract;

use App\Classes\Joke;

/**
 * Interface for Jokes importing.
 */

interface JokesInterface
{
    /**
     * @return array
     */
    public function getCategories(): array;

    /**
     * @param string $category
     * @return Joke|null
     */
    public function getRandomJoke(string $category = ''): ?Joke;
}