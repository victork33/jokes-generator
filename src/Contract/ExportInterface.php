<?php 

namespace App\Contract;

use App\Classes\Joke;

interface ExportInterface
{
    public function save(Joke $joke);
}